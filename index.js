'use strict'
require('dotenv').config()
const config = require('./config')
const express = require('express')
const bodyParser = require('body-parser')
const graphqlHTTP = require('express-graphql')
const mongoose = require('mongoose')

const schema = require('./graphql/schema');
const rootValue = require('./graphql/resolvers');
const isAuth = require('./middleware/is-auth');

const app = express()
app.use(bodyParser.json())

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Methods', 'POST,GET,OPTIONS')
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
  if (req.method === 'OPTIONS') {
    return res.sendStatus(200)
  }
  next()
})

app.use(isAuth)

app.use('/graphql', graphqlHTTP({
  schema,
  rootValue,
  graphiql: true,
}))

const mongodbUrl = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`

mongoose
  .connect(mongodbUrl, { useNewUrlParser: true })
  .then(() => {
    app.get('/', (req, res) => res.send(`Welcome to ${config.appName}`))
    app.listen(config.port, () => console.log(`Server running on port ${config.port}..`))
  })
  .catch(err => {
    console.log(err)
  })