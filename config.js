module.exports = {
  appName : process.env.APP_NAME || 'Kaokeb',
  port : process.env.APP_PORT || 8080
}