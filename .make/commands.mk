.DEFAULT_GOAL := help

.PHONY: help
help:
	@printf "Help:\n"
	@printf "install\n"

.PHONY: install
install: --copy-env --setup-network --build-container --install-dependencies

.PHONY: run
run:
	$(DOCKER_COMPOSE) up -d

.PHONY: stop
stop:
	$(DOCKER_COMPOSE) stop

.PHONY: logs
logs:
	$(DOCKER_COMPOSE_LOGS)

# Private Phony
--copy-env:
	@echo "Copy env if not existing"
	@cp -n .env.example .env

--setup-network:
	@(docker network ls --format "{{.Name}}" --filter Name=^$(APP_NETWORK)$ | grep ^$(APP_NETWORK)$ || docker network create $(APP_NETWORK))

--build-container:
	$(DOCKER_COMPOSE) build

--install-dependencies:
	$(DOCKER_COMPOSE_RUN_NODE) yarn install